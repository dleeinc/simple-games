
## Object:
*Be the first player to score 121 points.*

## Setup

Shuffle a 52 deck of cards. Each player picks a color and places both pegs of her color behind the Start line on the Cribbage board. Randomly select the first Dealer (the other player is called the Pone).


## Game Flow
1. The Dealer deals 6 cards to each player.
2. Each player selects 2 cards to send to the Dealer's "Crib," which is kept face down.
3. The Pone cuts the deck and reveals the top card, called the Starter Card. If this card is a Jack (His Nibs), the Dealer scores 2 points.
4. Players perform Pegging (starting with Pone)
5. Players Show their hands (starting with Pone)
6. The Dealer Shows her Crib.
7. Shuffle all 52 cards together.

The Dealer and Pone switch roles.
Repeat this process until the end of the game.

## Pegging
- Starting with the Pone player, players alternate playing cards, announcing the cumulative total of the cards played.
- If a player cannot play a card without exceeding 31, she instead says "Go." or ("I can't go anymore!" or "PASS")
- Once both players have said "Go," the last player to have played a card scores 1 point (or 2 points if she reached exactly 31 points).
- The other player starts the next 31-count sequence, starting at 0.

## Scoring
*During Pegging, score as follows*

- combinations of 15 - **2 points**
- 2 of a kind - **2 points**
- 3 of a kind - **6 points**
- 4 of a kind - **12 points**
-Run of X cards (>= 3) - **X points**
  - Must be the last 2/3/4 cards in the current 31-count sequence.
  - Must be the last X cards of the current sequence, but order does not matter.
- Your foremost peg represents your current score. To update your score, move the rearmost peg to reflect the new score.
- After both players have played all of their cards, players gather their hands back and proceed to The Show.

# The Show
Players reveal their hands and score as follows, counting the Starter Card as part of their hands (and Crib).
- The Pone scores her hand
- The Dealer
- then the Dealer's Crib.
- A single card may be used to fulfill any number of scoring conditions, as long as the exact same cards aren't used to fulfill the same scoring condition.

## Scoring

- 2/3/4 of a kind and Run of X cards - **same as Pegging** (2/6/12)
- Cards with a sum of exactly 15 - **2 points**
- The Jack matching the suit of the Starter Card (His Nobs) - **1 point**
- Flush of 4 in hand (not in Crib, not counting Starter Card) - **4 points**
- Flush of 5 in hand or Crib (including Starter Card) - **5 points**
- These cannot be scored simultaneously.
- Play proceeds until one player reaches or exceeds 121 points and wins the game!

## Links
[cribbage.org](http://www.cribbage.org/rules/rule1.asp)

[Hoyle](http://www.hoylegaming.com/rules/showrule.aspx?RuleID=207)
